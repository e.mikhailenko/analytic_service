# Generated by Django 3.2.5 on 2021-07-28 02:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('presets', '0003_remove_preset_token'),
        ('reports', '0009_input_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='input',
            name='file',
        ),
        migrations.AddField(
            model_name='input',
            name='preset',
            field=models.ManyToManyField(blank=True, null=True, related_name='inputs', to='presets.Preset'),
        ),
    ]
