import os

from django.db import models
from django.core.validators import FileExtensionValidator
from django.utils.timezone import now

from analyticService.settings import BASE_DIR, MEDIA_ROOT
from reports.general import delete_all_files_in_folder
from presets.models import Preset


def report_scripts_directory_path(instance, filename):
    return f'report_scripts/{instance.id}/{filename}'


def report_input_directory_path(instance, filename):
    return f'report_input/{instance.report.id}/{filename}'


def report_output_directory_path(instance, filename):
    return f'report_output/{instance.report.id}/{filename}'


def report_param_dict_directory_path(instance, filename):
    return f'report_param_dict/{filename}'


class Report(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    description = models.TextField(blank=True)
    script = models.FileField(
        upload_to=report_scripts_directory_path,
        validators=[FileExtensionValidator(['py'])],
        blank=True,
        null=True
    )
    presets = models.ManyToManyField(Preset, blank=True, related_name='reports')
    param_dicts = models.ManyToManyField('ParamDict', blank=True, related_name='reports')
    last_start = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def generate_report(self):
        script_path = f'{BASE_DIR}{self.script.url}'
        output_path = f'{MEDIA_ROOT}/report_output/{self.id}'
        if not os.path.exists(f'{MEDIA_ROOT}/report_output'):
            os.mkdir(f'{MEDIA_ROOT}/report_output')
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        context = {
            'output_model': Output,
            'errors_log_model': ErrorsLog,
            'report_log_model': ReportLog,
            'report_id': self.id,
            'output_path': output_path,
        }
        if Param.objects.filter(report_id=self.id):
            context['params_model'] = Param.objects.filter(report_id=self.id)
        if self.presets:
            presets_dict = {}
            for preset in self.presets.all():
                presets_dict[preset.name] = preset.output_file.path
            context['presets_dict'] = presets_dict
        if self.param_dicts:
            param_dicts = {}
            for param_dict in self.param_dicts.all():
                param_dicts[param_dict.name] = param_dict.file.path
            context['param_dicts'] = param_dicts
        delete_all_files_in_folder(output_path)
        Output.objects.filter(report_id=self.id).delete()
        exec(open(script_path).read(), None, context)
        self.last_start = now()
        self.save()


class Output(models.Model):
    name = models.CharField(max_length=200)
    file = models.FileField(upload_to=report_output_directory_path, blank=True, null=True)
    report = models.ForeignKey(
        Report,
        related_name='output',
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Param(models.Model):
    name = models.CharField(max_length=200)
    value = models.CharField(max_length=200)
    report = models.ForeignKey(
        Report,
        related_name='params',
        on_delete=models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class ParamDict(models.Model):
    name = models.CharField(max_length=200)
    file = models.FileField(upload_to=report_param_dict_directory_path)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class ErrorsLog(models.Model):
    name = models.CharField(max_length=200)
    body = models.TextField(blank=True)
    report = models.ForeignKey(
        Report,
        related_name='errors_logs',
        on_delete=models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        get_latest_by = 'created'


class ReportLog(models.Model):
    name = models.CharField(max_length=200)
    body = models.TextField(blank=True)
    report = models.ForeignKey(
        Report,
        related_name='report_logs',
        on_delete=models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        get_latest_by = 'created'
