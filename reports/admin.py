from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, reverse

from .models import Report, Output, Param, ParamDict, ErrorsLog, ReportLog


class ParamInline(admin.TabularInline):
    model = Param
    show_change_link = True
    extra = 1


class ParamDictInline(admin.TabularInline):
    model = ParamDict
    show_change_link = True
    extra = 1


class OutputInline(admin.TabularInline):
    model = Output
    readonly_fields = ['created']
    max_num = 1
    show_change_link = True


class ReportLogInline(admin.TabularInline):
    model = ReportLog
    max_num = 1

    def get_queryset(self, request):
        qs = super(ReportLogInline, self).get_queryset(request)
        latest_id = qs.latest().id
        return qs.filter(id=latest_id)


class ErrorsLogInline(admin.TabularInline):
    model = ErrorsLog
    max_num = 1

    def get_queryset(self, request):
        qs = super(ErrorsLogInline, self).get_queryset(request)
        latest_id = qs.latest().id
        return qs.filter(id=latest_id)


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'description', 'script', 'last_start', 'created', 'updated']
    list_filter = ['name', 'last_start', 'created']
    search_fields = ['name']
    inlines = [ParamInline, OutputInline, ReportLogInline, ErrorsLogInline]

    def get_urls(self):
        urls = super().get_urls()
        gen_report_urls = [
            path(
                'generate_report/<int:pid>',
                self.admin_site.admin_view(self.generate_report),
                name='generate_report'
            ),
        ]
        return gen_report_urls + urls

    @staticmethod
    def generate_report(self, pid=0):
        if pid != 0:
            report = Report.objects.get(pk=pid)
            if report:
                report.generate_report()
                return redirect(reverse("admin:reports_report_change", args=[pid]))
        return redirect(reverse("admin:reports_report_changelist"))


@admin.register(Output)
class OutputDataAdmin(admin.ModelAdmin):
    list_display = ['name', 'file', 'report', 'created', 'updated']
    list_filter = ['report__name', 'created']
    search_fields = ['name', 'report__name']


@admin.register(Param)
class ParamAdmin(admin.ModelAdmin):
    list_display = ['name', 'value', 'report', 'created', 'updated']
    list_filter = ['report__name', 'created']
    search_fields = ['name', 'report__name']


@admin.register(ParamDict)
class ParamDictAdmin(admin.ModelAdmin):
    list_display = ['name', 'file', 'created', 'updated']
    list_filter = ['created']
    search_fields = ['name']


@admin.register(ErrorsLog)
class ErrorsLogAdmin(admin.ModelAdmin):
    list_display = ['name', 'body', 'report', 'created', 'updated']
    list_filter = ['report__name', 'created']
    search_fields = ['name', 'report__name']


@admin.register(ReportLog)
class ReportLogAdmin(admin.ModelAdmin):
    list_display = ['name', 'body', 'report', 'created', 'updated']
    list_filter = ['report__name', 'created']
    search_fields = ['name', 'report__name']
