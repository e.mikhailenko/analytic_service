import requests
import pandas as pd
import csv


def get_df_from_request(url, headers, data):
    request = requests.get(url, headers=headers, data=data)
    if '200' not in str(request):
        return pd.DataFrame()
    decoded_content = request.content.decode('utf-8')
    cr = csv.reader(decoded_content.splitlines(), delimiter=';')
    df = pd.DataFrame(cr)

    df.columns = df.iloc[0]
    df.drop(index=0, inplace=True)

    try:
        df['created_at'] = pd.to_datetime(df['created_at'])
    except:
        pass
    try:
        df['date_created'] = pd.to_datetime(df['date_created'])
    except:
        pass
    if df.shape[0] == 0:
        print('нет данных')
    else:
        print('получено строк: ', df.shape[0])
    return df