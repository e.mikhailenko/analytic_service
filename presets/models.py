import json
import os

from django.db import models

from analyticService.settings import MEDIA_ROOT, TEXEL_TOKEN
from presets.general import get_df_from_request


def preset_output_file_directory_path(instance, filename):
    return f'preset_output/{instance.id}/{filename}'


class Preset(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    description = models.TextField(blank=True)
    url = models.CharField(max_length=500)
    output_file = models.FileField(
        upload_to=preset_output_file_directory_path,
        blank=True,
        null=True
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def generate_preset(self):
        preset_output_path = f'{MEDIA_ROOT}/preset_output'
        output_path = f'{preset_output_path}/{self.id}'
        if not os.path.exists(preset_output_path):
            os.mkdir(preset_output_path)
        if not os.path.exists(output_path):
            os.mkdir(output_path)
        data = {'Token': TEXEL_TOKEN}
        data = json.dumps(data, ensure_ascii=False)
        headers = {
            'Content-Type': 'application/json; charset=utf-8',
            'Token': TEXEL_TOKEN
        }
        url = self.url
        all_params = self.params.all()
        if all_params:
            for count, param in enumerate(all_params):
                if count == 0:
                    url += '?'
                if param.is_header_param:
                    headers[param.name] = param.value
                else:
                    url += param.name + '=' + param.value
                    if count != all_params.count():
                        url += '&'
        df = get_df_from_request(url, headers, data)
        if df.empty:
            print('Дата фрейм пуст')
        else:
            df.to_csv(f'{output_path}/{self.slug}.csv', sep=';')
            self.output_file = f'preset_output/{self.id}/{self.slug}.csv'
            self.save()


class PresetParam(models.Model):
    name = models.CharField(max_length=200)
    value = models.CharField(max_length=200)
    is_header_param = models.BooleanField(default=False)
    preset = models.ForeignKey(
        Preset,
        related_name='params',
        on_delete=models.CASCADE
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
