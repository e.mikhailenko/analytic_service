from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, reverse

from .models import Preset, PresetParam


class ParamPresetInline(admin.TabularInline):
    model = PresetParam
    show_change_link = True
    extra = 1


@admin.register(Preset)
class PresetAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'description', 'url', 'created', 'updated']
    list_filter = ['name', 'created']
    search_fields = ['name']
    inlines = [ParamPresetInline]

    def get_urls(self):
        urls = super().get_urls()
        gen_preset_urls = [
            path(
                'generate_preset/<int:pid>',
                self.admin_site.admin_view(self.generate_preset),
                name='generate_preset'
            ),
        ]
        return gen_preset_urls + urls

    @staticmethod
    def generate_preset(self, pid=0):
        if pid != 0:
            preset = Preset.objects.get(pk=pid)
            if preset:
                preset.generate_preset()
                return redirect(reverse("admin:presets_preset_change", args=[pid]))
        return redirect(reverse("admin:presets_preset_changelist"))


@admin.register(PresetParam)
class ParamPresetAdmin(admin.ModelAdmin):
    list_display = ['name', 'value', 'is_header_param', 'preset', 'created', 'updated']
    list_filter = ['preset__name', 'created']
    search_fields = ['name', 'preset__name']
